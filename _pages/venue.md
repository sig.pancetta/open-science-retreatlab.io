---
permalink: /venue/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/lake.jpg
author_profile: false
title: "Venue"
---

For now we can only tell you that the retreat will be at a lake and with a mountain view. It is south of Munich.

More soon...

