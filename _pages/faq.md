---
layout: splash
permalink: /faq/
title: "FAQ"
hidden: true
header:
  overlay_color: "#594777"
  overlay_image: "/assets/images/night.jpg"
excerpt: "Answers to your questions"
---

### Can I bring my loved ones to the retreat?

We are trying to be as inclusive as possible. We hope to make it possible for
you to bring kids/friends/partners/dogs/..., but we'll have to discuss the
specifics with you directly. 
Please [get in touch](https://open-science-retreat.gitlab.io/contact/).


### The event looks great, but I can't afford it. What can I do?

We will have as many stipends available as possible. We are currently
looking for sponsors and will send out info on stipends via the 
Open Science Retreat
[mailing list](https://www.getrevue.co/profile/open-science-retreat).


### I'd like to sponsor the event. How do I do that?

We'd love to get sponsoring to make it possible for everyone to join (also
those who cannot afford it). Please [get in
touch](https://open-science-retreat.gitlab.io/contact/).


### Can I arrive on Sunday?

Regular check-in is on Monday. Early arrival on Sunday is possible. 
You can go hiking, to the nearby spa, explore the lake and marshlands, or just
relax at the site.


### I have a question that was not answered here. What can I do?

Please [get in touch](https://open-science-retreat.gitlab.io/contact/).


---

## Don't miss any updates

Sign up for e-mail updates on the Open Science Retreat:

<div id="revue-embed">
  <form action="https://www.getrevue.co/profile/open-science-retreat/add_subscriber" method="post" id="revue-form" name="revue-form"  target="_blank">
  <div class="revue-form-group">
    <label for="member_email">Email address</label>
    <input class="revue-form-field" placeholder="Your email address..." type="email" name="member[email]" id="member_email">
  </div>
  <div class="revue-form-actions">
    <input type="submit" value="Subscribe" name="member[subscribe]" id="member_submit">
  </div>
  <div class="revue-form-footer">By subscribing, you agree with Revue’s <a target="_blank" href="https://www.getrevue.co/terms">Terms of Service</a> and <a target="_blank" href="https://www.getrevue.co/privacy">Privacy Policy</a>.</div>
  </form>
</div>

[View previous updates](https://www.getrevue.co/profile/open-science-retreat){: .btn .btn--success}
