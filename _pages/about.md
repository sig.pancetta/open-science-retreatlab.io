---
permalink: /about/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/lake.jpg
author_profile: false
title: "About"
---

The Open Science Retreat is an event from Open Science enthusiast for Open Science enthusiasts.
The main goals are:

- Learning
- Getting stuff done
- Networking
- Reflecting
- Rebooting

