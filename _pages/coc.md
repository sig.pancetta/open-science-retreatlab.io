---
permalink: /coc/
layout: splash
header:
  overlay_color: "#05bf85"
author_profile: false
title: "Code of conduct"
---

We are dedicated to creating an environment where everyone feels safe and welcome.

Our code of conduct will be published here shortly.

